#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

const char* ssid = ""; // WiFi SSID
const char* password = ""; // WIFI password

const float step_to_degree_ratio = 200.0 / 360.0; // steps per rotation
const int pitch_max = 350; // maximum value of pitch, this way the motor does not turn to far
const int yaw_max = 100; // maximum value of yaw, this way the motor does not turn to far
const int pitch_reset = 0; // value the pitch motor is set to when reset (move the motors to the marked positions)
const int yaw_reset = 0; // value the yaw motor is set to when reset (move the motors to the marked positions)
const int pitch_step_pin  = 0; //GPIO0---D3 of Nodemcu--Direction of pitch stepper motor driver
const int pitch_dir_pin = 2; //GPIO2---D4 of Nodemcu--Step of pitch stepper motor driver
const int yaw_step_pin  = 13; //GPI13---D7 of Nodemcu--Direction of yaw stepper motor driver
const int yaw_dir_pin = 15; //GPIO15---D8 of Nodemcu--Step of yaw stepper motor driver
int pitch = pitch_reset; // current pitch of the turret
int pitch_control = 0; // requested pitch
int yaw = yaw_reset; // current yaw of the turret
int yaw_control = 0; // requested yaw
int control_check_counter = 0; // counter to avoid polling the master on every loop cycle, once it reaches 0 a HTTP request will poll the master for new yaw and pitch control values
const int CONTROL_CHECK_COUNTER_MAX = 100000; // value the control_check_counter is reset to after it reached 0
bool reaching_master = false; // indicator if master is currently reachable
const int STEP_DELAY = 10; // delay in between high and low of a step
const int MAX_STEP_AMOUNT = 5; // maximum amount of steps the motors take per loop cycle
const char* YAW_CONTROL_JSON_KEY = "yaw"; // yaw JSON key in the masters response
const char* PITCH_CONTROL_JSON_KEY = "pitch"; // yaw JSON key in the masters response

const char* MASTER_URL = ""; // URL which the ESP polls control values from

WiFiServer server(80); // open HTTP server on port 80

// one time setup every time the esp starts
void setup() {
  // begin serial output with baudrate 1115200
  Serial.begin(115200);
  delay(10);
  // mark step and dir pins as output
  pinMode(pitch_step_pin, OUTPUT);
  pinMode(pitch_dir_pin,  OUTPUT);
  pinMode(yaw_step_pin, OUTPUT);
  pinMode(yaw_dir_pin,  OUTPUT);

  // Currently no movement or laser pointing
  digitalWrite(pitch_step_pin, LOW);
  digitalWrite(pitch_dir_pin, LOW);
  digitalWrite(yaw_step_pin, LOW);
  digitalWrite(yaw_dir_pin, LOW);

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.println(step_to_degree_ratio);
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.hostname("MueckenlaserESP");
  WiFi.begin(ssid, password);

  // print dots while WiFi is connecting
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address on serial monitor
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");    //URL IP to be typed in mobile/desktop browser
  Serial.print(WiFi.localIP());
  Serial.println("/");
  Serial.println();
}

// function to move a single axis closer to the given control value
int move_axis(int axis, int axis_control, int axis_dir_pin, int axis_step_pin) {
    int i = 0;
    bool direction = LOW;
    int steps = 0;

    if (axis < axis_control) {
        direction = HIGH;
        steps = axis_control - axis;
    }

    if (axis > axis_control) {
        direction = LOW;
        steps = axis - axis_control;
    }

    // if steps is bigger than MAX_STEP_AMOUNT it needs to be reduced to MAX_STEP_AMOUNT
    if (MAX_STEP_AMOUNT < steps){
        steps = MAX_STEP_AMOUNT;
    }

    // Rotate stepper motor in given direction
    digitalWrite(axis_dir_pin, direction);
    for(i=0; i<steps; i++) {
        // step pin needs to be activated and deactivated once for one step
        digitalWrite(axis_step_pin, HIGH);
        delay(STEP_DELAY);
        digitalWrite(axis_step_pin, LOW);
        delay(STEP_DELAY);
        // internal variable needs to be corrected to reflect motor movement
        if (direction == LOW){
            axis -= 1;
        }
        if (direction == HIGH){
            axis += 1;
        }
    }

    // return current axis status
    return axis;
}

// funtion to move both motors closer to the control values
void move_motors() {
    pitch = move_axis(pitch, pitch_control, pitch_dir_pin, pitch_step_pin);
    yaw = move_axis(yaw, yaw_control, yaw_dir_pin, yaw_step_pin);
}

// function to request new control values from master
void get_controls() {
    // to reduce the amount of HTTP requests to the master this counter is used
    // once it reaches 0 a HTTP request will be sent further down in the method
    // if it is not 0 yet it will be decreased by 1 and the method will return
    if (control_check_counter > 0) {
        control_check_counter -= 1;
        return;
    }
    // reset counter to skip the HTTP request the next times
    control_check_counter = CONTROL_CHECK_COUNTER_MAX;

    Serial.print("Getting new orders from master: ");
    Serial.println(MASTER_URL);

    // Call master control URL
    HTTPClient http;  //Object of class HTTPClient
    http.begin(MASTER_URL);
    // return code of HTTP request
    int httpCode = http.GET();
    // check if there is a http response code bigger than 0
    if (httpCode > 0) {
        // set status of reaching_master variable to indicate master is reachable
        reaching_master = true;
        // Parsing JSON
        DynamicJsonDocument jsonBuffer(1024);
        deserializeJson(jsonBuffer, http.getString());
        // read control values from JSON
        int pitch_control_degrees = jsonBuffer[PITCH_CONTROL_JSON_KEY];
        pitch_control = round(pitch_control_degrees * step_to_degree_ratio);
        int yaw_control_degrees = jsonBuffer[YAW_CONTROL_JSON_KEY];
        yaw_control = round(yaw_control_degrees * step_to_degree_ratio);
        // Output to serial monitor
        Serial.print("pitch_control:");
        Serial.println(pitch_control);
        Serial.print("yaw_control:");
        Serial.println(yaw_control);
        Serial.println();
    } else {
        // set status of reaching_master variable to indicate master is unreachable
        reaching_master = false;
        Serial.println("Couldn't reach master, pausing a few cycles");
        Serial.println();
        // set counter to high value to delay next HTTP call by ~10 seconds if master is unreachable
        control_check_counter += 600000;
    }
    //Close connection
    http.end();
}


String convert_boolean_to_string(bool boolean) {
    // convert boolean to a char*
    // simply calling String(reaching_master) would result in "0"/"1" instead of "false"/"true"
    char* string = "false";
    if(boolean) {
        string = "true";
    }
    return string;
}

// function to catch and respond to incoming HTTP requests
void check_for_incoming_http_requests() {
    // Check if a client has connected, otherwise return
    WiFiClient client = server.available();
    if (!client) {
        return;
    }

    // Wait until the client sends some data
    Serial.println("new client");
    while(!client.available()){
        delay(1);
    }

    // Read the first line of the request
    String request = client.readStringUntil('\r');
    Serial.println(request);
    client.flush();

    String reaching_master_string = convert_boolean_to_string(reaching_master);

    // actual HTTP response
    if(request.indexOf("/status") != -1){
        // Return the JSON response on /status path
        client.println("HTTP/1.1 200 OK");
        client.println("Content-Type: application/json");
        client.println(""); //  do not forget this one
        client.print("{");
        client.print("\"yaw\":" + String(yaw / step_to_degree_ratio) + ",");
        client.print("\"pitch\":" + String(pitch / step_to_degree_ratio) + ",");
        client.print("\"pitch_control\":" + String(pitch_control / step_to_degree_ratio) + ",");
        client.print("\"yaw_control\":" + String(yaw_control / step_to_degree_ratio) + ",");
        client.print("\"reaching_master\":" + reaching_master_string);
        client.println("}");
    } else {
        // Return the HTML response otherwise
        // This is not very useful as it is slow, therefore the whole if statement could be removed, which would result in always answering with the JSON version
        char* reaching_master_string_bootstrap_color = "danger";
        if (reaching_master) {
            reaching_master_string_bootstrap_color = "success";
        }

        char* pitch_color = "warning";
        if (pitch == pitch_control) {
            pitch_color = "success";
        }

        char* yaw_color = "warning";
        if (yaw == yaw_control) {
            yaw_color = "success";
        }

        client.println("HTTP/1.1 200 OK");
        client.println("Content-Type: text/html");
        client.println(""); //  do not forget this one
        client.println("<!DOCTYPE HTML>");
        client.println("<html>");
        client.println(    "<head>");
        client.println(         "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">");
        client.println(    "</head>");
        client.println(    "<body>");
        client.println(         "<div class=\"text-center\">");
        client.println(             "<h1 class=\"h1 font-weight-normal\">Laser Turret Motor Controller</h1>");
        client.println(             "<img src=\"https://github.com/mueckenlaser/images/raw/dd7154703fb00ab4d2b6b89e0b3c809e10b7d58e/logo.png\" width=\"290\" height=\"290\">");
        client.println(         "</div>");
        client.println(         "<div style=\"min-width:300px; max-width:400px; width: 50%; margin: 0 auto;\" class=\"card\">");
        client.println(             "<table class=\"table table-striped\">");
        client.println(                 "<tr>");
        client.println(                     "<td>Pitch</td><td><span class=\"badge badge-pill badge-" + String(pitch_color) + "\">" + String(pitch) + "</span></td>");
        client.println(                 "</tr><tr>");
        client.println(                     "<td>Pitch Control</td><td><span class=\"badge badge-pill badge-success\">" + String(pitch_control) + "</span></td>");
        client.println(                 "</tr><tr>");
        client.println(                     "<td>Yaw</td><td><span class=\"badge badge-pill badge-" + String(yaw_color) + "\">" + String(yaw) + "</span></td>");
        client.println(                 "</tr><tr>");
        client.println(                     "<td>Yaw Control</td><td><span class=\"badge badge-pill badge-success\">" + String(yaw_control) + "</span></td>");
        client.println(                 "</tr><tr>");
        client.println(                     "<td>Reaching Master</td><td><span class=\"badge badge-pill badge-" + String(reaching_master_string_bootstrap_color) + "\">" + String(reaching_master_string) + "</span></td>");
        client.println(                 "</tr>");
        client.println(             "</table>");
        client.println(         "</div>");
        client.println(    "</body>");
        client.println("</html>");
    }
    delay(1);
    Serial.println("Client disonnected");
    Serial.println("");
}

// loop which gets run after setup() automatically over and over again
void loop() {
  check_for_incoming_http_requests();
  get_controls();
  move_motors();
}
